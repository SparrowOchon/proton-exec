# proton-exec

Small script to allow for any number of third party executables to run in the same prefix as a steam application simultaneously.<br>As well as executing wine pre-installed utilities under system32 such as winecfg,winedbg,regedit...etc. or directly in the users path such as winetricks.

## Features:

- Automatically use the same Proton Version as is selected in Steam.
  - Loop through previous versions if steam selected version is invalid
- Automatically identify Steam Runtime location.
- Automatically identify SteamLibraries.
- Allows you to run any installed program by simply typing the program name ex `./proton-exec <AppID> <Locally Installed Program>`
- Allow for any number of parameters to be passed.
- Real time log data being written to the screen on run.
- Allow for multiple programs to run at the same time in the Prefix.
- Minimalistic script KISS principle.

## Usage

Launch the application/game you want to connect to directly from steam. You can select any proton-version directly through steam for the application/game in question. **The order only matters for the first launch as proton will create missing directories**.Once launched, proceed to launch the script with the appropriate information as denoted below;


- **First parameter** must be the Steam App ID or Steam Game Id
- **Second parameter** must be an exec which you are trying to run under the same prefix as the App/Game
- **Third parameter onward** can be parameters for the exe if needed

### External Exe files

This applies to any exe file not pre-installed under wine.

```
proton-exec <appid> <exe> <exe parameters>
```

- NOTE: The `exe` <b>MUST</b> contain the exectuables extension. I.e `path/to/file.exe`

### Wine Utilities/Local Execs

This applies to any utility/exec installed under wine or the users system path (winetricks,winedbg,winecfg,regedit,notepad,taskmgr...etc)

```
proton-exec <appid> <utility name> <util parameters>
```

- Lookup priority for Utilities and Local System Apps:

  - Inside Prefix's System32 file
  - Local users Path

- NOTE: The `utility name` <b>SHOULD NOT</b> contain the extension I.e no `.exe` or `.msi` in name


## Installation 
There is no Installation required.<br> No additional setup required.
